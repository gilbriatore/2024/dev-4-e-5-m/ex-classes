
import java.util.Scanner;
import dominio.Aluno;
import dominio.Calculadora;
import dominio.Bike;


public class Programa {



    public static void main(String[] args) {
        

        //Uso de uma biblioteca de funções (static)
        double resSoma = Calculadora.somar(10, 5);
        double resSub = Calculadora.subtrair(10, 5);
        Double numObjeto = 10.5;
        //Double nDouble = new Double("100");
        String strNum = "100";
        double numero = Double.parseDouble(strNum);
        
        //Aluno.nome = "João";
        Aluno aluno1obj = new Aluno("João");
        //aluno1obj.nome = "João";

        Aluno aluno2obj = new Aluno("Ana", "123");
        //aluno2obj.nome = "Ana";
        //aluno2obj.matricula = "123";

        Aluno aluno3obj = new Aluno();

        System.out.println("Nome 1: " + aluno1obj.getNome());
        System.out.println("Nome 2: " + aluno2obj.getNome());




    }



    public static void main2(String[] args) {
        

        //Uso de uma biblioteca de funções (static)

        double a = 100;
        double b = 50;
        double r = Calculadora.somar(a, b);




        //Exemplo de uso de objeto + método
        Bike objBike = new Bike();
        objBike.trocarDeMarcha(5);

        System.out.println("Marcha: " + objBike.numeroMarcha);

    }


    public static void oldMain(String[] args){
        
        Scanner leitor = new Scanner(System.in);

        //Um aluno
        // String matricula;
        // String nome;
        // double nota1;
        // double nota2;
        //Aluno objAluno = new Aluno();
        // System.out.println("Digite a matrícula");
        // objAluno.matricula = leitor.nextLine();

        // System.out.println("Digite o nome");
        // objAluno.nome = leitor.nextLine();

        // System.out.println("Digite a nota 1");
        // objAluno.nota1 = leitor.nextDouble();

        // System.out.println("Digite a nota 2");
        // objAluno.nota2 = leitor.nextDouble();

        // System.out.println("Matrícula: " + objAluno.matricula);
        // System.out.println("Nome: " + objAluno.nome);
        // System.out.println("Nota 1: " + objAluno.nota1);
        // System.out.println("Nota 2: " + objAluno.nota2);

        //Cinco alunos
        int qtde = 5;
        // String[] matriculas = new String[qtde];
        // String[] nomes = new String[qtde];
        // double[] notas1 = new double[qtde];
        // double[] notas2 = new double[qtde];
        Aluno[] alunos = new Aluno[qtde];
        alunos[0] = new Aluno();

        System.out.println("Digite a matrícula");
        alunos[0].matricula = leitor.nextLine();

        System.out.println("Digite o nome");
        //alunos[0].nome = leitor.nextLine();

        System.out.println("Digite a nota 1");
        alunos[0].nota1 = leitor.nextDouble();

        System.out.println("Digite a nota 2");
        alunos[0].nota2 = leitor.nextDouble();

        System.out.println("Matrícula: " + alunos[0].matricula);
        //System.out.println("Nome: " + alunos[0].nome);
        System.out.println("Nota 1: " + alunos[0].nota1);
        System.out.println("Nota 2: " + alunos[0].nota2);

        leitor.close();

    }


}
