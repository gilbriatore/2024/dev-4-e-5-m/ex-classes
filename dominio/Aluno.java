package dominio;

public class Aluno {

    public String matricula;
    
    private String nome;

    public String getNome(){
        return nome;
    }

    //Método construtor
    public Aluno(){
     
    }

    //Método construtor com parâmetro
    public Aluno(String nome){
        this.nome = nome;        
    }

    //Método construtor com parâmetros
    public Aluno(String nome, String matricula){
        this.nome = nome;
        this.matricula = matricula;
    }



    public double nota1;
    public double nota2;

}
